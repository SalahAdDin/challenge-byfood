package main

import "fmt"

func getMostFrequentTerm(items []string) string {
	histogram := make(map[string]int)
	for _, word := range items {
		histogram[word]++
	}

	max_count := 0
	aux := ""
	for count_a, count_b := range histogram {
		if max_count < count_b {
			max_count = count_b
			aux = count_a
		}
	}

	return aux
}

func main() {
	words := []string{"apple", "pie", "apple", "red", "red", "red"}

	fmt.Printf(getMostFrequentTerm((words)))
}
