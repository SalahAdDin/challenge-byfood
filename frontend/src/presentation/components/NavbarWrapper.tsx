import React, { FC } from "react";

const NavbarWrapper: FC<React.PropsWithChildren> = ({ children }) => {
  return (
    <nav className="rounded border-gray-200 bg-white px-2 py-2.5 dark:bg-gray-900 sm:px-4">
      <div className="container mx-auto flex flex-wrap items-center justify-between">
        {children}
      </div>
    </nav>
  );
};

export default NavbarWrapper;
