import React, { FC } from "react";
import { Controller } from "react-hook-form";

import { InputProps } from "@domain/inputs.dto";

const Input: FC<InputProps> = ({
  name,
  className,
  control,
  label,
  placeholder = "",
  required,
  type = "text",
}) => (
  <Controller
    name={name}
    control={control}
    render={({ field, fieldState: { error } }) => (
      <div className={className}>
        <input
          type={type}
          id={name}
          className={`border-1 peer block w-full appearance-none rounded-lg bg-transparent px-2.5 pb-2.5 pt-4 text-sm text-gray-900 focus:outline-none focus:ring-0 dark:text-white ${
            error
              ? "border-red-600 focus:border-red-600 dark:border-red-500 dark:focus:border-red-500"
              : "border-gray-300 bg-transparent focus:border-gray-600 dark:border-gray-600  dark:focus:border-gray-500"
          }`}
          placeholder={placeholder}
          required={required}
          {...field}
        />
        <label
          htmlFor={name}
          className={`absolute top-2 left-1 z-10 origin-[0] -translate-y-4 scale-75 transform bg-white px-2 text-sm duration-300 peer-placeholder-shown:top-1/2 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:scale-100 peer-focus:top-2 peer-focus:-translate-y-4 peer-focus:scale-75 peer-focus:px-2  dark:bg-gray-900  ${
            error
              ? "text-red-600 peer-focus:text-red-600 dark:text-red-500 peer-focus:dark:text-red-500"
              : "text-gray-500 peer-focus:text-gray-600 dark:text-gray-400 peer-focus:dark:text-gray-500"
          }`}
        >
          {label}
        </label>
        {error && (
          <p className="mt-2 text-sm text-red-600 dark:text-red-400">
            {error.message}
          </p>
        )}
      </div>
    )}
  />
);

export default Input;
