import { Outlet } from "@tanstack/react-router";

function App() {
  return (
    <div className="container my-8 max-w-screen-md">
      <Outlet />
    </div>
  );
}

export default App;
