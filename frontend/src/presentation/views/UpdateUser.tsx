import { FC } from "react";
import { useForm } from "react-hook-form";

import userSchema from "@application/user.validation";
import { IBaseUser, IUser } from "@domain/user.dto";
import { DevTool } from "@hookform/devtools";
import { yupResolver } from "@hookform/resolvers/yup";
import service from "@infrastructure/api/user.service";
import Error from "@presentation/components/Error";
import NavbarWrapper from "@presentation/components/NavbarWrapper";
import Spinner from "@presentation/components/Spinner";
import Input from "@presentation/components/forms/inputs/Input";
import TextArea from "@presentation/components/forms/inputs/TextArea";
import router from "@presentation/router";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useParams } from "@tanstack/react-router";

const initialValues: Partial<IBaseUser> = {
  firstName: "",
  lastName: "",
  mail: "",
  phone: "",
  bio: "",
};

const UpdateUser: FC = () => {
  const { userId } = useParams();

  const queryClient = useQueryClient();

  const { reset, control, handleSubmit } = useForm<IBaseUser>({
    defaultValues: initialValues,
    mode: "onChange",
    resolver: yupResolver(userSchema),
  });

  const {
    data,
    error,
    isError,
    isLoading: loadingUser,
  } = useQuery<IUser, Error>({
    queryKey: ["user", userId || ""],
    queryFn: () => service.getUser(userId),
    onSuccess: (data) => reset(data),
    enabled: !!userId,
  });

  const { mutate, isLoading } = useMutation({
    mutationFn: (formData: IBaseUser) => {
      if (!!userId) {
        return service.updateUser({ id: userId, ...formData });
      }
      return service.createUser(formData);
    },
    onSuccess: () => {
      reset();
      queryClient.invalidateQueries({ queryKey: ["users"] });
      router.history.back();
    },
  });

  const onSubmit = (data: IBaseUser) => {
    mutate(data);
  };

  return (
    <>
      <NavbarWrapper>
        <div className="inline-flex gap-4">
          <button
            type="button"
            className="inline-flex items-center rounded-full border border-gray-700 p-2.5 text-center text-sm font-medium text-gray-700 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-4 focus:ring-gray-300 dark:border-gray-500 dark:text-gray-500 dark:hover:text-white dark:focus:ring-gray-800"
            onClick={() => {
              router.history.back();
            }}
          >
            <svg
              aria-hidden="true"
              className="h-5 w-5  rotate-180"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                clipRule="evenodd"
              ></path>
            </svg>
            <span className="sr-only">Go back</span>
          </button>
          <h1 className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
            Users
          </h1>
        </div>
      </NavbarWrapper>

      <div className="relative mt-4 overflow-x-auto border sm:rounded-lg">
        {isError && <Error message={error.message} />}
        {userId && loadingUser && (
          <div className="flex h-[75vh] w-full flex-col items-center justify-center">
            <Spinner />
          </div>
        )}
        {!isError && !(userId && loadingUser) && (
          <form className="m-6" onSubmit={handleSubmit(onSubmit)}>
            <div className="grid md:grid-cols-2 md:gap-6">
              <Input
                className="group relative z-0 mb-4 w-full"
                control={control}
                name="firstName"
                label="First Name"
                required
              />
              <Input
                className="group relative z-0 mb-4 w-full"
                control={control}
                name="lastName"
                label="Last Name"
                required
              />
              <Input
                className="group relative z-0 mb-4 w-full"
                control={control}
                type="email"
                name="mail"
                label="E-Mail"
                required
              />
              <Input
                className="group relative z-0 mb-4 w-full"
                control={control}
                type="tel"
                name="phone"
                label="Phone"
                required
              />
              <TextArea
                className="group relative z-0 col-span-2 mb-4 w-full"
                control={control}
                name="bio"
                label="Biography"
              />
            </div>
            <div className="float-right my-4 inline-flex gap-2">
              <button
                type="reset"
                className="rounded-lg border border-gray-300 bg-white px-5 py-2.5 text-sm font-medium text-gray-900 hover:bg-gray-100 focus:outline-none focus:ring-4 focus:ring-gray-200 dark:border-gray-600 dark:bg-gray-800 dark:text-white dark:hover:border-gray-600 dark:hover:bg-gray-700 dark:focus:ring-gray-700"
                onClick={() => {
                  reset();
                  router.history.back();
                }}
              >
                Cancel
              </button>
              <button
                type="submit"
                className="w-full rounded-lg bg-gray-700 px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-gray-800 focus:outline-none focus:ring-4 focus:ring-gray-300 dark:bg-gray-600 dark:hover:bg-gray-700 dark:focus:ring-gray-800 sm:w-auto"
              >
                {isLoading && (
                  <svg
                    aria-hidden="true"
                    role="status"
                    className="mr-3 inline h-4 w-4 animate-spin text-white"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="#E5E7EB"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentColor"
                    />
                  </svg>
                )}
                Submit
              </button>
            </div>
          </form>
        )}
        <DevTool control={control} />
      </div>
    </>
  );
};

export default UpdateUser;
