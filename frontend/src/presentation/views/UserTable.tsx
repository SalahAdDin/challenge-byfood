import { FC, useState } from "react";

import { IUser } from "@domain/user.dto";
import service from "@infrastructure/api/user.service";
import Error from "@presentation/components/Error";
import NavbarWrapper from "@presentation/components/NavbarWrapper";
import Spinner from "@presentation/components/Spinner";
import router, { editRoute } from "@presentation/router";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";

const UserTable: FC = () => {
  const queryClient = useQueryClient();

  const [selectedUserId, setSelectedUserId] = useState<string>("");

  const {
    data,
    error,
    isError,
    isLoading: loadingUsers,
  } = useQuery<IUser[], Error>({
    queryKey: ["users"],
    queryFn: service.getAllUsers,
  });

  const { mutate, isLoading: deletingUser } = useMutation({
    mutationFn: () => service.deleteUser(selectedUserId),
    onSuccess: () => queryClient.invalidateQueries({ queryKey: ["users"] }),
  });

  return (
    <>
      <NavbarWrapper>
        <h1 className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
          Users
        </h1>

        <div className="inline-flex rounded-md shadow-sm" role="group">
          <button
            type="button"
            className="rounded-l-lg border border-gray-200 bg-white py-2 px-4 text-sm font-medium text-gray-900 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:text-blue-700 focus:ring-2 focus:ring-blue-700 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:hover:bg-gray-600 dark:hover:text-white dark:focus:text-white dark:focus:ring-blue-500"
            onClick={() => {
              router.navigate({ to: "/update" });
            }}
          >
            New
          </button>
          <button
            type="button"
            className="border-t border-b border-gray-200 bg-white py-2 px-4 text-sm font-medium text-gray-900 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:text-blue-700 focus:ring-2 focus:ring-blue-700 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:hover:bg-gray-600 dark:hover:text-white dark:focus:text-white dark:focus:ring-blue-500"
            onClick={() => {
              if (selectedUserId !== "")
                router.navigate({
                  to: editRoute.id,
                  params: {
                    userId: selectedUserId,
                  },
                });
            }}
          >
            Edit
          </button>
          <button
            type="button"
            className="rounded-r-md border border-gray-200 bg-white py-2 px-4 text-sm font-medium text-gray-900 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:text-blue-700 focus:ring-2 focus:ring-blue-700 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:hover:bg-gray-600 dark:hover:text-white dark:focus:text-white dark:focus:ring-blue-500"
            disabled={selectedUserId === ""}
            onClick={() => mutate()}
          >
            {deletingUser && (
              <svg
                role="status"
                className="mr-2 inline h-4 w-4 animate-spin text-gray-200 dark:text-gray-600"
                viewBox="0 0 100 101"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                  fill="currentColor"
                />
                <path
                  d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                  fill="#1C64F2"
                />
              </svg>
            )}
            Delete
          </button>
        </div>
      </NavbarWrapper>

      <div className="relative mt-4 overflow-x-auto border sm:rounded-lg">
        {isError && <Error message={error.message} />}
        {loadingUsers && (
          <div className="flex h-[75vh] w-full flex-col items-center justify-center">
            <Spinner />
          </div>
        )}
        {data && (
          <table className="w-full text-left text-sm text-gray-500 dark:text-gray-400">
            <thead className="bg-gray-50 text-xs uppercase text-gray-700 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                <th scope="col" className="p-4">
                  <div className="flex items-center"></div>
                </th>
                <th scope="col" className="py-3 px-6">
                  First Name
                </th>
                <th scope="col" className="py-3 px-6">
                  Last Name
                </th>
                <th scope="col" className="py-3 px-6">
                  Mail
                </th>
                <th scope="col" className="py-3 px-6">
                  Phone
                </th>
              </tr>
            </thead>
            <tbody>
              {data.map((user) => (
                <tr
                  className="border-b bg-white hover:bg-gray-50 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-600"
                  key={user.id}
                >
                  <td className="w-4 p-4">
                    <div className="flex items-center">
                      <input
                        id={`checkbox-table-${user.id}`}
                        type="checkbox"
                        className="h-4 w-4 rounded border-gray-300 bg-gray-100 text-blue-600 focus:ring-2 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600"
                        onChange={() =>
                          setSelectedUserId((prevState) =>
                            prevState === user.id ? "" : user.id,
                          )
                        }
                        checked={user.id === selectedUserId}
                      />
                      <label htmlFor="checkbox-table-1" className="sr-only">
                        Select User
                      </label>
                    </div>
                  </td>
                  <th
                    scope="row"
                    className="whitespace-nowrap py-4 px-6 font-medium text-gray-900 dark:text-white"
                  >
                    {user.firstName}
                  </th>
                  <td className="py-4 px-6">{user.lastName}</td>
                  <td className="py-4 px-6">{user.mail}</td>
                  <td className="py-4 px-6">{user.phone}</td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    </>
  );
};

export default UserTable;
