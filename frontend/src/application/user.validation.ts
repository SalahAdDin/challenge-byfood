import { SchemaOf, object, string } from "yup";

import { IBaseUser } from "@domain/user.dto";

const TURKISH_PHONE_NUMBER_PATTERN =
  /^(05)[0-9][0-9][\s]([0-9]){3}([\s]([0-9]){2}){2}/;

const userSchema: SchemaOf<IBaseUser> = object({
  firstName: string()
    .required("Please enter your first name.")
    .min(3, "It must have minimum 3 letters"),
  lastName: string()
    .required("Please enter your last name.")
    .min(3, "It must have minimum 3 letters"),
  mail: string()
    .required("Please enter your e-mail address.")
    .email("Please insert a valid e-mail."),
  phone: string()
    .required("Please enter your phone number.")
    .matches(
      TURKISH_PHONE_NUMBER_PATTERN,
      "Please insert a valid phone number.",
    ),
  bio: string()
    .required("Please enter your biography.")
    .min(250, "It must have minimum 250 letters"),
});

export default userSchema;
