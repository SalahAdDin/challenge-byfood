interface IBaseUser {
  firstName: string;
  lastName: string;
  mail: string;
  phone: string;
  bio: string;
}

interface IUser extends IBaseUser {
  readonly id: string;
}

export type { IBaseUser, IUser };
