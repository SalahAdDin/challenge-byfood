interface BaseInputProps {
  name: string;
  control: any;
  label: string;
  required?: boolean | undefined;
  placeholder?: string | undefined;
  className?: string | undefined;
}

interface InputProps extends BaseInputProps {
  type?: string | undefined;
  // setValue?: any;
}

interface TextAreaProps extends BaseInputProps {
  rows?: number | undefined;
}

export type { InputProps, TextAreaProps };
