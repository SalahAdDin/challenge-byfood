import { IBaseUser, IUser } from "@domain/user.dto";

import client from "./client";
import endpoints from "./user.endpoints";

const getAllUsers = async () => {
  const { data } = await client.get(endpoints.all);

  return data;
};

const getUser = async (id: string) => {
  const { data } = await client.get(`${endpoints.byId}${id}`);

  return data;
};

const createUser = async (user: IBaseUser) => {
  const { data } = await client.post(endpoints.create, user);

  return data;
};

const updateUser = async (user: IUser) => {
  const { data } = await client.patch(endpoints.update, user);

  return data;
};

const deleteUser = async (id: string) => {
  const { status } = await client.delete(`${endpoints.byId}${id}`);

  return status;
};

const service = { deleteUser, getAllUsers, getUser, createUser, updateUser };

export default service;
