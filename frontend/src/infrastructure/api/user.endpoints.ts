const endpoints = {
  all: "/users",
  byId: "/user/",
  create: "/user/create",
  update: "/user/update",
};

export default endpoints;
