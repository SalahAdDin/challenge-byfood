package main

import (
	"fmt"
	"sort"
	"strings"
	"unicode/utf8"
)

func sortByLetterFrequency(input []string) {
	type stat struct{ count, length int }

	calculated_stats := map[string]stat{}

	for _, word := range input {
		calculated_stats[word] = stat{
			count:  strings.Count(word, "a"),
			length: utf8.RuneCountInString(word),
		}
	}

	sort.Slice(input, func(i, j int) bool {
		word_a, word_b := calculated_stats[input[i]], calculated_stats[input[j]]

		if word_a.count != word_b.count {
			return word_a.count > word_b.count
		}

		return word_a.length > word_b.length
	})

}

func main() {
	words := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}

	sortByLetterFrequency(words)

	fmt.Println(words)
}
