package main

import "fmt"

func magicFunction(n int) int {

	fmt.Println(n)

	/*
		TODO:
		if n == 0 {
			return 2
		}
		return n * magicFunction(n-1) */
}

func main() {
	fmt.Println(magicFunction(9))
}
